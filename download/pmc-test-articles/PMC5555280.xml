<article xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:mml="http://www.w3.org/1998/Math/MathML" article-type="editorial">
  <?properties open_access?>
  <front>
    <journal-meta>
      <journal-id journal-id-type="nlm-ta">Int J Womens Dermatol</journal-id>
      <journal-id journal-id-type="iso-abbrev">Int J Womens Dermatol</journal-id>
      <journal-title-group>
        <journal-title>International Journal of Women's Dermatology</journal-title>
      </journal-title-group>
      <issn pub-type="epub">2352-6475</issn>
      <publisher>
        <publisher-name>Elsevier</publisher-name>
      </publisher>
    </journal-meta>
    <article-meta>
      <article-id pub-id-type="pmid">28831420</article-id>
      <article-id pub-id-type="pmc">5555280</article-id>
      <article-id pub-id-type="publisher-id">S2352-6475(17)30052-7</article-id>
      <article-id pub-id-type="doi">10.1016/j.ijwd.2017.06.003</article-id>
      <article-categories>
        <subj-group subj-group-type="heading">
          <subject>Editorial</subject>
        </subj-group>
      </article-categories>
      <title-group>
        <article-title>The International Journal of Women&#8217;s Dermatology is now listed on PubMed Central<sup><xref ref-type="fn" rid="d31e14">&#9734;</xref></sup></article-title>
      </title-group>
      <contrib-group>
        <contrib contrib-type="author">
          <name>
            <surname>Murrell</surname>
            <given-names>Dedee F</given-names>
          </name>
          <degrees>MA, MBMCh, MD (UNSW), FAAD, FACD, FRCP(Edin)</degrees>
          <email>d.murrell@unsw.edu.au</email>
          <xref rid="af0005" ref-type="aff">a</xref>
          <xref rid="af0010" ref-type="aff">b</xref>
          <xref rid="cr0005" ref-type="corresp">&#8270;</xref>
        </contrib>
        <contrib contrib-type="author">
          <name>
            <surname>Grant-Kels</surname>
            <given-names>Jane</given-names>
          </name>
          <degrees>MD, FAAD</degrees>
          <xref rid="af0015" ref-type="aff">c</xref>
        </contrib>
      </contrib-group>
      <aff id="af0005"><label>a</label>Department of Dermatology, St. George Hospital, Sydney, Australia</aff>
      <aff id="af0010"><label>b</label>Faculty of Medicine, University of New South Wales, Sydney, Australia</aff>
      <aff id="af0015"><label>c</label>Department of Dermatology, University of Connecticut Health Center, Farmington, CT</aff>
      <author-notes>
        <corresp id="cr0005"><label>&#8270;</label>Corresponding author. <email>d.murrell@unsw.edu.au</email></corresp>
      </author-notes>
      <pub-date pub-type="pmc-release">
        <day>08</day>
        <month>7</month>
        <year>2017</year>
      </pub-date>
      <!-- PMC Release delay is 0 months and 0 days and was based on <pub-date
						pub-type="epub">.-->
      <pub-date pub-type="collection">
        <month>9</month>
        <year>2017</year>
      </pub-date>
      <pub-date pub-type="epub">
        <day>08</day>
        <month>7</month>
        <year>2017</year>
      </pub-date>
      <volume>3</volume>
      <issue>3</issue>
      <fpage>126</fpage>
      <lpage>126</lpage>
      <permissions>
        <copyright-statement>&#169; 2017 The Authors</copyright-statement>
        <copyright-year>2017</copyright-year>
        <license license-type="CC BY-NC-ND" xlink:href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
          <license-p>This is an open access article under the CC BY-NC-ND license (http://creativecommons.org/licenses/by-nc-nd/4.0/).</license-p>
        </license>
      </permissions>
    </article-meta>
  </front>
  <body>
    <p>Excellent news arrived on May 12, 2017 at the Elsevier office in Amsterdam: the flagship journal of the Women&#8217;s Dermatologic Society (WDS), the International Journal of Women&#8217;s Dermatology (IJWD), had just been accepted on PubMed Central (PMC). That very day, all authors who had published papers in its 10 quarterly issues since early 2015 received individual emails from PubMed Central informing them that their paper had been indexed.</p>
    <p>The process of a new journal to become indexed on PMC is a lengthy one that assesses each journal according to a number of criteria (<xref rid="bb0010" ref-type="bibr">US National Library of Medicine, 2017</xref>). Scientific content is the most important aspect, which IJWD passed on its first and only application. Other aspects include the incorporation of conflicts of interest, employing an international editorial board of editors with good track records, evidence of genuine peer reviews, and the listing of grant funding. PMC also has technical requirements that each journal must meet.</p>
    <p>The IJWD editorial team and the WDS are delighted that the IJWD is listed on PMC. This means that researchers will become more aware of content that is relevant to them. Because IJWD is an open-access journal, researchers will be able to download the full version of articles without the need for passwords or subscriptions.</p>
    <p>One of our stumbling blocks in gaining more submissions up until now has been that we were not listed on PubMed, a database of citations and abstracts for more than 27 million articles. PMC is an electronic archive of full-text journal articles that offers free access to its contents. PMC contains more than 4 million articles, most of which have a corresponding entry in PubMed (<xref rid="bb0015" ref-type="bibr">US National Library of Medicine National Institutes of Health, 2015</xref>). Those concerned should now feel comfortable in submitting articles to our journal. The IJWD has a worldwide reach as the metadata shows and because we are an open access journal, there are fees if the article is accepted for publication. These fees are one quarter of the fees that are charged by predatory journals (<xref rid="bb0005" ref-type="bibr">Murrell, 2016</xref>) and are currently US$800 for research papers and US$500 for case reports and letters. These fees are much less than those charged by other legitimate journals if you wish to make your article open access (typically approximately US $3,000).</p>
    <p>Our next aim is to gain an impact factor higher than 1. We hope that prospective authors will now feel more comfortable about publishing in our journal.</p>
  </body>
  <back>
    <ref-list id="bi0005">
      <title>References</title>
      <ref id="bb0005">
        <element-citation publication-type="journal" id="rf0005">
          <person-group person-group-type="author">
            <name>
              <surname>Murrell</surname>
              <given-names>D.F.</given-names>
            </name>
          </person-group>
          <article-title>A lesson learned about predatory journals and their difference from peer-reviewed open-access publishing</article-title>
          <source>Int J Womens Dermatol</source>
          <volume>13;2</volume>
          <issue>4</issue>
          <year>2016</year>
          <fpage>113</fpage>
          <lpage>114</lpage>
        </element-citation>
      </ref>
      <ref id="bb0010">
        <element-citation publication-type="other" id="rf0010">
          <person-group person-group-type="author">
            <name>
              <surname>US National Library of Medicine</surname>
            </name>
          </person-group>
          <article-title>FAQ: Journal Selection for MEDLINE<sup>&#174;</sup> Indexing at NLM [Internet]</article-title>
          <comment>[cited 2017 May 28]. Available from:</comment>
          <ext-link ext-link-type="uri" xlink:href="https://www.nlm.nih.gov/pubs/factsheets/j_sel_faq.html" id="ir0005">https://www.nlm.nih.gov/pubs/factsheets/j_sel_faq.html</ext-link>
          <year>2017</year>
        </element-citation>
      </ref>
      <ref id="bb0015">
        <element-citation publication-type="other" id="rf0015">
          <person-group person-group-type="author">
            <name>
              <surname>US National Library of Medicine National Institutes of Health</surname>
            </name>
          </person-group>
          <article-title>PMC FAQs [Internet]</article-title>
          <comment>[cited 2017 May 28]. Available from:</comment>
          <ext-link ext-link-type="uri" xlink:href="https://www.ncbi.nlm.nih.gov/pmc/about/faq/" id="ir0010">https://www.ncbi.nlm.nih.gov/pmc/about/faq/</ext-link>
          <year>2015</year>
        </element-citation>
      </ref>
    </ref-list>
    <fn-group>
      <fn id="d31e14">
        <label>&#9734;</label>
        <p id="np0005">Conflicts of interest: The authors declare no conflict of interest.</p>
      </fn>
    </fn-group>
  </back>
</article>
