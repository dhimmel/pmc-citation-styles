<article xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:mml="http://www.w3.org/1998/Math/MathML" article-type="case-report">
  <?properties open_access?>
  <front>
    <journal-meta>
      <journal-id journal-id-type="nlm-ta">Indian J Urol</journal-id>
      <journal-id journal-id-type="iso-abbrev">Indian J Urol</journal-id>
      <journal-id journal-id-type="publisher-id">IJU</journal-id>
      <journal-title-group>
        <journal-title>Indian Journal of Urology : IJU : Journal of the Urological Society of India</journal-title>
      </journal-title-group>
      <issn pub-type="ppub">0970-1591</issn>
      <issn pub-type="epub">1998-3824</issn>
      <publisher>
        <publisher-name>Medknow Publications &amp; Media Pvt Ltd</publisher-name>
        <publisher-loc>India</publisher-loc>
      </publisher>
    </journal-meta>
    <article-meta>
      <article-id pub-id-type="pmid">23671373</article-id>
      <article-id pub-id-type="pmc">3649608</article-id>
      <article-id pub-id-type="publisher-id">IJU-29-73</article-id>
      <article-id pub-id-type="doi">10.4103/0970-1591.109992</article-id>
      <article-categories>
        <subj-group subj-group-type="heading">
          <subject>Case Report</subject>
        </subj-group>
      </article-categories>
      <title-group>
        <article-title>Laparoscopic management in a rare case of bilateral perirenal lymphangiomatosis</article-title>
      </title-group>
      <contrib-group>
        <contrib contrib-type="author">
          <name>
            <surname>Meyyappan</surname>
            <given-names>R. M.</given-names>
          </name>
          <xref ref-type="aff" rid="aff1"/>
          <xref ref-type="corresp" rid="cor1"/>
        </contrib>
        <contrib contrib-type="author">
          <name>
            <surname>Ravikumar</surname>
            <given-names>S.</given-names>
          </name>
          <xref ref-type="aff" rid="aff1"/>
        </contrib>
        <contrib contrib-type="author">
          <name>
            <surname>Gopinath</surname>
            <given-names>M.</given-names>
          </name>
          <xref ref-type="aff" rid="aff1"/>
        </contrib>
      </contrib-group>
      <aff id="aff1">Department of Urology, Madras Medical College and Rajiv Gandhi Govt. General Hospital, Chennai, India</aff>
      <author-notes>
        <corresp id="cor1"><bold>For correspondence:</bold> Prof. Meyyappan RM, Department of Urology, Madras Medical College and Rajiv Gandhi Govt. General Hospital, Park town, Chennai - 600 003, Tamil Nadu, India. E-mail: <email xlink:href="menauro@gmail.com">menauro@gmail.com</email></corresp>
      </author-notes>
      <pub-date pub-type="ppub">
        <season>Jan-Mar</season>
        <year>2013</year>
      </pub-date>
      <volume>29</volume>
      <issue>1</issue>
      <fpage>73</fpage>
      <lpage>74</lpage>
      <permissions>
        <copyright-statement>Copyright: &#169; Indian Journal of Urology</copyright-statement>
        <copyright-year>2013</copyright-year>
        <license license-type="open-access" xlink:href="http://creativecommons.org/licenses/by-nc-sa/3.0">
          <license-p>This is an open-access article distributed under the terms of the Creative Commons Attribution-Noncommercial-Share Alike 3.0 Unported, which permits unrestricted use, distribution, and reproduction in any medium, provided the original work is properly cited.</license-p>
        </license>
      </permissions>
      <abstract>
        <p>Lymphangiomas are lymphatic malformations commonly seen around the neck, axillary region and the mediastinum. Perirenal lymphangiomatosis is very rare with only a few cases being reported in literature. We present a case of symptomatic perirenal lymphangiomatosis in a female of childbearing age, managed laparoscopically by deroofing and marsupialization.</p>
      </abstract>
      <kwd-group>
        <kwd>Laparoscopy</kwd>
        <kwd>lymphangiomatosis</kwd>
        <kwd>marsupialization</kwd>
      </kwd-group>
    </article-meta>
  </front>
  <body>
    <sec id="sec1-1">
      <title>INTRODUCTION</title>
      <p>Perirenal lymphangiomatosis or renal lymphangiectasia is a rare condition with varied clinical presentations. This condition must always be kept in mind when a perirenal fluid collection is encountered, particularly as a bilateral presentation. Symptomatic cases are treated either by percutaneous drainage, marsupialization (open or laparoscopic) or by nephrectomy, depending upon the presentation.</p>
    </sec>
    <sec id="sec1-2">
      <title>CASE REPORT</title>
      <p>A 23-year-old female presented with complaints of bilateral loin pain, more on the right side, for the past 3 months. The pain was dull aching, continuous and non-radiating. On examination, her vitals were stable. Per-abdominal examination showed a vague non-tender mass palpable in the right lumbar region. Her routine urine and blood investigations were found to be within normal limits. Ultrasonogram (USG) showed bilateral perirenal collection with septations and the collection was seen to extend into the renal sinuses [<xref ref-type="fig" rid="F1">Figure 1a</xref>]. The kidneys were normal and the corticomedullary differentiations were maintained on both sides. Computed tomography (CT) scan of abdomen showed bilateral perirenal hypodense multiloculated collection, with density varying between 8 and 12 HU [<xref ref-type="fig" rid="F1">Figure 1b</xref>]. Delayed films showed normal excretion of the contrast with splaying of the pelvi-calyceal system with intervening fluid in the sinuses. No extravasation of the contrast was noted into the collection. Magnetic resonance imaging (MRI) showed a perirenal hyperintense collection in T2W images and also in the renal sinus [<xref ref-type="fig" rid="F1">Figure 1c</xref>]. A diagnosis of perirenal lymphangiectasis was made. She underwent laparoscopic deroofing and marsupialization of the perirenal collection on either side [Figure <xref ref-type="fig" rid="F2">2a</xref> and <xref ref-type="fig" rid="F2">b</xref>]. She had an uneventful recovery but for a persistent drain which subsided by the 12<sup>th</sup> day postoperatively. The deroofed specimen showed thin-walled multiple cysts which on histological examination were found to be lined by thin endothelial cells. The patient made a complete recovery and is on regular follow-up for the past 18 months [<xref ref-type="fig" rid="F2">Figure 2c</xref>].</p>
      <fig id="F1" position="float">
        <label>Figure 1</label>
        <caption>
          <p>(a) USG showing perirenal hypoechoic collection. (b) CECT &#8211; Renal parenchymal enhancement with perirenal and renal sinus hypodense nonenhancing collection. (c) MRI T2W coronal section showing bilateral perirenal hyperintense collection with septae</p>
        </caption>
        <graphic xlink:href="IJU-29-73-g001"/>
      </fig>
      <fig id="F2" position="float">
        <label>Figure 2</label>
        <caption>
          <p>(a) Perirenal multicystic lesion dissected. (b) Lap deroofing in progress. (c) Postoperative CT scan (delayed film)</p>
        </caption>
        <graphic xlink:href="IJU-29-73-g002"/>
      </fig>
    </sec>
    <sec sec-type="discussion" id="sec1-3">
      <title>DISCUSSION</title>
      <p>Renal lympnangiectasis is a rare disorder in which lymphangiectasia follows the failure of developing perirenal lymphatic tissue to establish normal communication with the remainder of the lymphatic system.[<xref ref-type="bibr" rid="ref1">1</xref>] Abnormal lymphatic channels may dilate to form a cystic mass that may be unilocular on multilocular. The origin of this disorder is speculative. There is a familial association in some cases, which argues for a congenital cause.[<xref ref-type="bibr" rid="ref2">2</xref>] Some argue for an acquired cause, which suggests that the lymphatic vessels may become blocked owing to inflammation or other obstruction and so cause lymphatic ectasia.[<xref ref-type="bibr" rid="ref3">3</xref>] Though known by different names &#8211; &#8220;renal lymphangiomatosis&#8221;, &#8220;renal lymphangioma&#8221;, &#8220;peripelvic lymphangiectasia&#8221;, and &#8220;renal peripelvic multicystic lymphangiectasia&#8221; &#8211; &#8220;renal lymphangiectasia&#8221; is the preferred name, given that the disorder is characterized by ectatic perirenal, peripelvic, and intrarenal lymphatic vessels.[<xref ref-type="bibr" rid="ref4">4</xref>] Patient symptoms described in the literature include flank pain, abdominal pain and rarely hematuria. The condition has been found in children and in adults. Large perinephric fluid collections have been found and are reported to be exacerbated during pregnancy.[<xref ref-type="bibr" rid="ref2">2</xref>]</p>
      <p>USG shows multiloculated, cystic perirenal and parapelvic collections with thin septations. The kidneys may be normal or enlarged in size. It has to be differentiated from hydronephrosis and polycystic kidney disease. CT scans show hypodense collections in the perirenal and parapelvic spaces with septations, with the parenchyma being normal. No abnormal enhancement is seen within the collections. Splaying of the renal calyceal systems is seen in delayed films. On CT scan, the presence of fluid attenuation in the range of 0&#8211;10 HU with absence of enhancement excludes other entities such as nephroblastomatosis and lymphoma. The other differential diagnoses of perirenal fluid collections are urinoma, hematoma and abscess. All these can be differentiated from lymphangiomatosis on the basis of whether the disease is bilateral or unilateral, the condition of the underlying parenchyma and the attenuation and enhancement patterns of the collections.</p>
      <p>MRI shows multiple hyperintense collections with septations on T2W images with reversal of corticomedullary intensity,[<xref ref-type="bibr" rid="ref5">5</xref>] due to an anatomic variation in the lymphatics, with abundance around the interlobar and arcuate blood vessels at the corticomedullary junction, fewer small lymphatics in the mid-cortex region and absence of lymphatics in the medulla region.</p>
      <p>The diagnosis of renal lymphangiectasia can be confirmed with needle aspiration of chylous fluid from the perinephric fluid collections. The presence of renin is more specific and confirms the renal origin of the collections. However, the ultrasonographic and CT findings are characteristic for this disease and allow the diagnosis to be made confidently. If asymptomatic, treatment is not usually necessary, and complicated cases may be treated by percutaneous drainage, or marsupialization or nephrectomy. Due to the multicystic nature of the lesion, there is a chance of recurrence after surgical procedures.</p>
      <p>In the case presented here, the diagnosis of renal lymphangiectasia was made based on the characteristic radiological findings. Since the patient was symptomatic on both sides and considering her age and possibility of exacerbation during pregnancy, it was decided on to intervene in the case. Due to the chances of failure and recurrence after percutaneous aspiration and due to the multiloculated nature of the collection, she underwent a laparoscopic deroofing and marsupialization so as to allow the lymph to be absorbed by the peritoneal surface.</p>
    </sec>
  </body>
  <back>
    <fn-group>
      <fn fn-type="supported-by">
        <p><bold>Source of Support:</bold> Nil</p>
      </fn>
      <fn fn-type="conflict">
        <p><bold>Conflict of Interest:</bold> None declared.</p>
      </fn>
    </fn-group>
    <ref-list>
      <title>REFERENCES</title>
      <ref id="ref1">
        <label>1</label>
        <element-citation publication-type="journal">
          <person-group person-group-type="author">
            <name>
              <surname>Murray</surname>
              <given-names>KK</given-names>
            </name>
            <name>
              <surname>McLellan</surname>
              <given-names>GL</given-names>
            </name>
          </person-group>
          <article-title>Renal peripelvic lymphangiectasia: Appearance at CT</article-title>
          <source>Radiology</source>
          <year>1991</year>
          <volume>180</volume>
          <fpage>455</fpage>
          <lpage>6</lpage>
          <pub-id pub-id-type="pmid">2068311</pub-id>
        </element-citation>
      </ref>
      <ref id="ref2">
        <label>2</label>
        <element-citation publication-type="journal">
          <person-group person-group-type="author">
            <name>
              <surname>Meredith</surname>
              <given-names>WT</given-names>
            </name>
            <name>
              <surname>Levine</surname>
              <given-names>E</given-names>
            </name>
            <name>
              <surname>Ahlstom</surname>
              <given-names>NG</given-names>
            </name>
            <name>
              <surname>Grantham</surname>
              <given-names>JJ</given-names>
            </name>
          </person-group>
          <article-title>Exacerbation of familial renal lymphangiomatosis during pregnancy</article-title>
          <source>AJR Am J Roentgenol</source>
          <year>1988</year>
          <volume>151</volume>
          <fpage>965</fpage>
          <lpage>6</lpage>
          <pub-id pub-id-type="pmid">3263029</pub-id>
        </element-citation>
      </ref>
      <ref id="ref3">
        <label>3</label>
        <element-citation publication-type="journal">
          <person-group person-group-type="author">
            <name>
              <surname>Kutcher</surname>
              <given-names>R</given-names>
            </name>
            <name>
              <surname>Mahadevia</surname>
              <given-names>P</given-names>
            </name>
            <name>
              <surname>Nussbaum</surname>
              <given-names>MK</given-names>
            </name>
            <name>
              <surname>Rosenblatt</surname>
              <given-names>R</given-names>
            </name>
            <name>
              <surname>Freed</surname>
              <given-names>S</given-names>
            </name>
          </person-group>
          <article-title>Renal peripelvic multicystic lymphangiectasia</article-title>
          <source>Urology</source>
          <year>1987</year>
          <volume>30</volume>
          <fpage>177</fpage>
          <lpage>9</lpage>
          <pub-id pub-id-type="pmid">3617306</pub-id>
        </element-citation>
      </ref>
      <ref id="ref4">
        <label>4</label>
        <element-citation publication-type="book">
          <person-group person-group-type="author">
            <name>
              <surname>Levine</surname>
              <given-names>E</given-names>
            </name>
          </person-group>
          <person-group person-group-type="editor">
            <name>
              <surname>Haaga</surname>
              <given-names>JR</given-names>
            </name>
            <name>
              <surname>Lanzieri</surname>
              <given-names>CF</given-names>
            </name>
            <name>
              <surname>Sartoris</surname>
              <given-names>DJ</given-names>
            </name>
            <name>
              <surname>Zerhouni</surname>
              <given-names>EA</given-names>
            </name>
          </person-group>
          <article-title>The kidney</article-title>
          <source>Computed tomography and magnetic resonance imaging of the whole body</source>
          <year>1994</year>
          <edition>3rd ed</edition>
          <publisher-loc>St Louis, Mo</publisher-loc>
          <publisher-name>Mosby</publisher-name>
          <fpage>1199</fpage>
        </element-citation>
      </ref>
      <ref id="ref5">
        <label>5</label>
        <element-citation publication-type="journal">
          <person-group person-group-type="author">
            <name>
              <surname>Mani</surname>
              <given-names>NB</given-names>
            </name>
            <name>
              <surname>Sodhi</surname>
              <given-names>KS</given-names>
            </name>
            <name>
              <surname>Singh</surname>
              <given-names>P</given-names>
            </name>
            <name>
              <surname>Katariya</surname>
              <given-names>S</given-names>
            </name>
            <name>
              <surname>Poddar</surname>
              <given-names>U</given-names>
            </name>
            <name>
              <surname>Thapa</surname>
              <given-names>BR</given-names>
            </name>
          </person-group>
          <article-title>Renal Lymphangiomatosis: A rare cause of bilateral nephromegaly</article-title>
          <source>Australas Radiol</source>
          <year>2003</year>
          <volume>47</volume>
          <fpage>184</fpage>
          <lpage>7</lpage>
          <pub-id pub-id-type="pmid">12780450</pub-id>
        </element-citation>
      </ref>
    </ref-list>
  </back>
</article>
