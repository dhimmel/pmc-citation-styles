# Citation styles in PubMed Central articles

Repository for the _Satoshi Village_ blog post titled [**On author versus numeric citation styles**](http://blog.dhimmel.com/citation-styles/) published on March 12, 2018.

This repository analyzes citation styles, i.e. author-style like `(Pantcheva, 2018)` or numeric-style like `[1]`.
Articles are analyzed from the [PMC Open Access Subset](https://www.ncbi.nlm.nih.gov/pmc/tools/openftlist/).

## Figures

To combine the two SVG figures side by side to create [`years-mutlipanel.svg`](years-mutlipanel.svg), run:

```sh
svg_stack.py \
  --direction=horizontal \
  --margin=0 \
  figure/years-absolute.svg figure/years-normalized.svg > \
  figure/years-mutlipanel.svg
```

This relies on [`varnion/svg_stack`](https://github.com/varnion/svg_stack), a fork of [`astraw/svg_stack`](https://github.com/astraw/svg_stack) with Python 3 support ([see PR](https://github.com/astraw/svg_stack/pull/7)).
While ideally this would be executed by `05.visualize.ipynb`, that was not working.
I also tried [`svgutils`](https://github.com/btel/svg_utils), but it was buggy and did not work.

To create a side-by-side PNG, run:

```sh
convert +append -gravity South \
  figure/years-absolute.png \
  figure/years-normalized.png \
  figure/years-mutlipanel.png
```

## Environment

This repository uses [conda](http://conda.pydata.org/docs/) to manage its environment as specified in [`environment.yml`](environment.yml).
Install the environment with:

```sh
conda env create --file=environment.yml
```

Then use `conda activate pmc-citation-styles` and `conda deactivate` to activate or deactivate the environment.

## License

All original content in this repository is dedicated to the public domain under Creative Commons Zero 1.0 (see [`LICENSE.md`](LICENSE.md)).
Note that this repository contains machine-readable formats of scholarly articles.
These articles are part of the PubMed Central Open Access Subset under a variety of licenses, many of which do not meet the [Open Definition](http://opendefinition.org/licenses/).
Refer to specific XML files for the license of that article as well as [`oa_file_list.csv`](`download/oa_file_list.csv.gz`), which has a column for auto-detected license information.

Please attribute this repository as appropriate if you reuse any parts of it.
While we won't claim copyright infringement if you don't, we still expect that users will follow scholarly norms that encourage linking to and attributing sources.
