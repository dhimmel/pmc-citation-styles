import hashlib
import logging
import pathlib
import random
import re
import statistics
import tqdm
import warnings
import zipfile

import bs4
import lxml.etree


def read_article(zip_file, filename):
    """
    Read a single XML file (filename) inside of an open zip_file.
    """
    xml_bytes = zip_file.read(filename)
    root = lxml.etree.fromstring(xml_bytes)
    return root


first_int_pattern = re.compile(r'\d+')


def get_first_int(text):
    """
    Get the first integer appearing in the string text
    """
    return int(first_int_pattern.search(text).group())


def read_articles(path, max_articles=None, randomize=None):
    """
    Return a generator of (filename, root) tuples for each article
    in pmc-articles-xml.zip. If randomize is none, do not shuffle articles.
    Otherwise, use randomize as a seed to shuffle articles.
    """
    with zipfile.ZipFile(path) as zip_file:
        filenames = zip_file.namelist()
        filenames.sort(key=get_first_int)
        if randomize is not None:
            seeded = random.Random(randomize)
            seeded.shuffle(filenames)
        if max_articles is not None:
            filenames = filenames[:max_articles]
        for filename in tqdm.tqdm_notebook(filenames):
            try:
                root = read_article(zip_file, filename)
            except Exception as e:
                logging.warning(f'Could not parse {filename}:\n{e}')
                continue
            yield filename, root


def first_or_none(x):
    """
    x is a list. If the list has any elements, return the text of the first,
    after stripping whitespace. Otherwise, return None.
    """
    return x[0].text.strip() if x else None


def get_article_info(root):
    """
    For an XML root object representing a PMC article, return article information.
    """
    info = {}
    # Article identifiers
    for id_type in 'pmc', 'pmid', 'doi':
        results = root.xpath(f"/article/front/article-meta/article-id[@pub-id-type='{id_type}']")
        id_ = first_or_none(results)
        if id_type == 'pmc' and id_ and not id_.startswith('PMC'):
            id_ = 'PMC' + id_
        info[id_type] = id_
    # Year first published
    years = root.xpath(f"/article/front/article-meta/pub-date/year")
    info['year'] = min(int(x.text) for x in years) if years else None
    # Journal
    journal_a = root.xpath("/article/front/journal-meta/journal-title-group/journal-title")
    journal_b = root.xpath("/article/front/journal-meta/journal-title")
    info['journal'] = first_or_none(journal_a) or first_or_none(journal_b)
    nlm_journal = root.xpath("/article/front/journal-meta/journal-id[@journal-id-type='nlm-ta']")
    info['nlm_journal'] = first_or_none(nlm_journal)
    # Publisher
    publisher = root.xpath("/article/front/journal-meta/publisher/publisher-name")
    info['publisher'] = first_or_none(publisher)
    return info


def get_citation_style_info(root):
    """
    For an XML root object representing a PMC article, deduce citation style
    indicators.
    """
    info = dict()
    cites = root.xpath("/article/body//xref[@ref-type='bibr']")
    info['n_cites'] = len(cites)
    cite_calls = {
        'number': [],
        'author': [],
        'unknown': [],
    }
    references = root.xpath("/article/back/ref-list/ref")
    n_references = len(references)

    for cite in cites:
        # Use itertext incase reference is embedded in children of <xref>
        text = ''.join(cite.itertext())
        call = characterize_cite(text, n_references=n_references or 999)
        cite_calls[call].append(text)
    info['cite_calls'] = cite_calls
    for call, cite_texts in cite_calls.items():
        info[f'n_{call}_cites'] = len(cite_texts)
    info['mode_style'], n = max(cite_calls.items(), key=lambda x: len(x[1]))

    info['n_references'] = n_references
    ref_labels = root.xpath("/article/back/ref-list/ref/label")
    ref_labels = [x.text for x in ref_labels]
    info['n_reference_labels'] = len(ref_labels)
    return info


"""
Helpers for character_cite
"""
strip_pattern = re.compile(r'^\W+|\W+$')
digit_pattern = re.compile(r'\d+')
# https://stackoverflow.com/a/6314634/4651668
letter_pattern = re.compile(r'[^\W\d_]')
# Suppress BeautifulSoup URL warnings
# https://stackoverflow.com/a/41496131/4651668
warnings.filterwarnings('ignore', category=UserWarning, module='bs4')


def characterize_cite(text, n_references=999):
    """
    Infer whether a citation is using a numbered or author-year style.
    """
    if text is None:
        return 'unknown'

    # Remove HTML formatting such as <sup></sup>
    soup = bs4.BeautifulSoup(text, 'lxml')
    text = soup.get_text()

    # Strip non-word characters from head/tail
    stripped_text = strip_pattern.sub(repl='', string=text)
    if not stripped_text:
        # Devoid of content
        return 'unknown'

    if len(stripped_text) == 1 and letter_pattern.fullmatch(stripped_text):
        # Single letter citations like [a] or [A].
        return 'unknown'

    # Track pieces of evidence for citaiton style
    number_evidence = set()
    author_evidence = set()

    # length
    if len(stripped_text) <= 3:
        number_evidence.add('short_length')

    # letters
    n_letters = sum(map(len, letter_pattern.findall(stripped_text)))
    if n_letters > 0:
        author_evidence.add('contains_letters')
    else:
        number_evidence.add('no_letters')

    # digits (this methods starts to cause problems when there are over 999 references)
    digits = digit_pattern.findall(text)
    if not digits:
        author_evidence.add('no_numbers')
    else:
        numbers = list()
        for d in digits:
            try:
                numbers.append(int(d))
            except ValueError:
                continue
        if numbers and statistics.median(numbers) <= n_references:
            number_evidence.add('median_number <= n_references')
        elif numbers:
            author_evidence.add('median_number > n_references')
        if numbers and all(x > n_references for x in numbers):
            # Tie-breaker for year only citations
            author_evidence.add('all_numbers > n_references')

    n_digits = sum(map(len, digits))
    if n_digits and n_letters:
        if n_digits > n_letters:
            number_evidence.add('more digits than letters')
        elif n_letters > n_digits:
            author_evidence.add('more letters than digits')

    # consensus
    if len(number_evidence) > len(author_evidence):
        return 'number'
    if len(number_evidence) < len(author_evidence):
        return 'author'
    return 'unknown'


def sha256sum(path):
    """
    Get the SHA-256 checksum of a pathlib.Path file.
    Based on https://stackoverflow.com/a/44873382/4651668
    """
    path = pathlib.Path(path)
    h = hashlib.sha256()
    with path.open(mode='rb', buffering=0) as f:
        for b in iter(lambda: f.read(128 * 1024), b''):
            h.update(b)
    return h.hexdigest()
